function transformData(data) {
    return data.map(item => ({
      id_user: item.userId,
      monday: item.sleepTimeDetails.monday,
      tuesday: item.sleepTimeDetails.tuesday,
      wednesday: item.sleepTimeDetails.wednesday,
      thursday: item.sleepTimeDetails.thursday,
      friday: item.sleepTimeDetails.friday,
      saturday: item.sleepTimeDetails.saturday,
      sunday: item.sleepTimeDetails.sunday
    }));
  }
  
  // Example usage
  const inputData = [
    {
      userId: 'Users/124916',
      sleepTimeId: 'SleepTimes/124919',
      sleepTimeDetails: {
        _key: '124919',
        _id: 'SleepTimes/124919',
        _rev: '_hhg9rvi---',
        active: false,
        monday: '00:00',
        tuesday: '00:00',
        wednesday: '00:00',
        thursday: '00:00',
        friday: '00:00',
        saturday: '00:00',
        sunday: '00:00'
      }
    },
    {
      userId: 'Users/126186',
      sleepTimeId: 'SleepTimes/126189',
      sleepTimeDetails: {
        _key: '126189',
        _id: 'SleepTimes/126189',
        _rev: '_hhhLvZi---',
        active: false,
        monday: '00:00',
        tuesday: '00:00',
        wednesday: '00:00',
        thursday: '00:00',
        friday: '00:00',
        saturday: '00:00',
        sunday: '00:00'
      }
    }
  ];
  
  const transformedData = transformData(inputData);
  console.log(transformedData);