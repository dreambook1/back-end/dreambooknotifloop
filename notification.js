var amqp = require('amqplib/callback_api');

async function sendNotification(user_idp, templatep, notifTypep) {
    amqp.connect(process.env.RABBIT_MQ, function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
            throw error1;
            }
            var queue = 'dreambook';
            var msg = {user_id: user_idp, template: templatep, notifType: notifTypep};

            channel.assertQueue(queue, {
            durable: false
            });

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)));
            console.log(" [x] Sent %s", msg);
        });
    });
}

module.exports = sendNotification ;
