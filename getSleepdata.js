const { Database, aql } = require('arangojs');

async function getSleepdata(global_pool) {
    const db = new Database({
      url: process.env.ARANGODB_URL, 
      databaseName: process.env.ARANGODB_DB, 
      auth: { username: process.env.ARANGODB_USERNAME, password: process.env.ARANGODB_PASSWORD }, 
    });
  
    try {
      const query1 = aql`FOR user IN Users
      FOR edge IN users_sleeptimes
        FILTER edge._from == user._id
        FOR sleepTime IN SleepTimes
          FILTER edge._to == sleepTime._id
          RETURN {
            userId: user._id,
            sleepTimeId: sleepTime._id,
            sleepTimeDetails: sleepTime
          }
    `;
      const cursor1 = await db.query(query1);
      const result1 = await cursor1.all();
      return result1
    } catch (error) {
      console.error('Error executing queries:', error);
    }
}
module.exports = getSleepdata;
