function convertToNextTimestamps(schedule) {
    const daysOfWeek = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  
    // Helper function to get the next occurrence of a time
    function getNextOccurrence(hour, minute, dayOffset) {
      const now = new Date();
      const targetTime = new Date(now);
      targetTime.setDate(now.getDate() + dayOffset); // Move to the correct day
      targetTime.setHours(hour, minute, 0, 0); // Set the target time
  
      // If the target time has already passed, move to the next week
      if (targetTime <= now) {
        targetTime.setDate(targetTime.getDate() + 7);
      }
  
      return Math.floor(targetTime.getTime() / 1000); // Convert to Unix timestamp
    }
  
    const result = {};
  
    schedule.forEach(userData => {
      const userId = userData.id_user;
      const userSchedule = {};
      daysOfWeek.forEach(day => {
        const time = userData[day];
        if (time) {
          const [hour, minute] = time.split(':').map(Number);
          const now = new Date();
          const todayIndex = now.getDay(); // Sunday is 0, Monday is 1, and so on.
          const targetDayIndex = daysOfWeek.indexOf(day);
          let dayOffset = targetDayIndex - todayIndex;
          if (dayOffset < 0) dayOffset += 7; // Ensure it's in the future
          userSchedule[day] = getNextOccurrence(hour, minute, dayOffset);
        }
      });
      result[userId] = userSchedule;
    });
    return result;
  }
  module.exports = convertToNextTimestamps;