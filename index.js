const fillGlobalPool = require("./fillGlobalPool")
const sendNotification = require("./notification")
require('dotenv').config()


const daysOfWeek = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
var globalPool = {}
const oneDayInMilliseconds = 24 * 60 * 60 * 1000;
var firstIntervalCompleted = false;

async function fillGlobalPoolOnce() {
    try {
        globalPool = await fillGlobalPool(globalPool);
        console.log("Global pool filled");
        firstIntervalCompleted = true;
    } catch (error) {
        console.error('Error filling global pool:', error);
    }
}

setInterval(() => {
    if (!firstIntervalCompleted) {
        fillGlobalPoolOnce();
    }
}, 1000);

setTimeout(() => {
    fillGlobalPoolOnce();
    clearInterval(firstInterval);
}, oneDayInMilliseconds);

setInterval(() => {
    for (item in globalPool) {
        const currentDay = daysOfWeek[new Date().getDay()];
        const timeDifference = globalPool[item][currentDay] - (new Date().getHours() * 60 * 60 * 1000 + new Date().getMinutes() * 60 * 1000);

        if (timeDifference < oneDayInMilliseconds) {
            sendNotification(item, "test", "device").then(() => {
                delete globalPool[item][currentDay];
            });
        }
    }
}, 60000);