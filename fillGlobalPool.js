const { Database, aql } = require('arangojs');
const convertToNextTimestamps = require('./utils/convertToNextTimestamps')
const getSleepdata = require('./getSleepdata')

require('dotenv').config()

function sanitizeData(data) {
    return data.map(item => ({
      id_user: item.userId,
      monday: item.sleepTimeDetails.monday,
      tuesday: item.sleepTimeDetails.tuesday,
      wednesday: item.sleepTimeDetails.wednesday,
      thursday: item.sleepTimeDetails.thursday,
      friday: item.sleepTimeDetails.friday,
      saturday: item.sleepTimeDetails.saturday,
      sunday: item.sleepTimeDetails.sunday
    }));
  }


async function fillGlobalPool(global_pool) {
  const db = new Database({
    url: process.env.ARANGODB_URL, 
    databaseName: process.env.ARANGODB_DB, 
    auth: { username: process.env.ARANGODB_USERNAME, password: process.env.ARANGODB_PASSWORD }, 
  });

  try {
    return await getSleepdata("qzd").then(data =>{
      const sanitizedData = sanitizeData(data)
      global_pool = convertToNextTimestamps(sanitizedData)
      return global_pool  
    })
  } catch (error) {
    console.error('Error fetching sleep data:', error);
    throw error;
  }
}


module.exports = fillGlobalPool;
